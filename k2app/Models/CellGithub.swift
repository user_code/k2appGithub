import UIKit
import Cosmos

extension UIImageView{
    func load(url:URL){
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf:url){
                if let image = UIImage(data: data){
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
            
        }
    }
}

extension Int {
    var roudedWidthAbbreviations : String {
        
        let number = Double(self)
        let thousand = number / 1000
        let million = number / 100000
        
        if million > 1.0{
            return "\(round(million*10)/10)M"
        }else if thousand > 1.0{
            return "\(round(thousand*10)/10)K"
        }else{
            return "\(self)"
        }
    }
}

class CellGithub: UICollectionViewCell {
    
    var github : RepositoryGithub? {
        didSet{
            guard let reporGit = github else { return }
            
            name.text = reporGit.name
            repository.text = reporGit.repository
            avatar.load(url: URL(string: reporGit.avatar)!)
            labelStar.text = reporGit.star.roudedWidthAbbreviations
        }
    }
    
    let avatar : UIImageView = {
        let image = UIImageView(image: UIImage(named: "avatar"))
        image.layer.masksToBounds = true
        image.layer.cornerRadius = 10
        return image
    }()
    
    let name : UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.text = "Alamofire"
        label.font = UIFont.systemFont(ofSize: 18)
        return label
    }()
    
    let repository : UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.text = "Alamofire/Alamofire"
        label.font = UIFont.systemFont(ofSize: 12)
        return label
    }()
    
    let star : CosmosView = {
       let cosmos = CosmosView()
       cosmos.settings.filledImage = UIImage(named: "RadingStarFilled")?.withRenderingMode(.alwaysOriginal)
       cosmos.settings.emptyImage = UIImage(named: "RadingStartEmpty")?.withRenderingMode(.alwaysOriginal)
        
       cosmos.settings.totalStars = 1
       cosmos.settings.starSize = 15
       cosmos.settings.starMargin = 3
        
       return cosmos
    }()
    
    let chip : UIView = {
       let ch = UIView()
        ch.layer.masksToBounds = true
        ch.layer.borderWidth = 1
        ch.layer.borderColor = UIColor(red: 27 / 255, green: 31 / 255, blue: 35 / 255, alpha: 0.1).cgColor
        ch.layer.cornerRadius = 6
        return ch
    }()
    
    let labelStar : UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 10)
        label.text = "10k"
        label.textColor = .black
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("fatal Error")
    }
    
    func setupLayout(){
        
        addSubview(avatar)
        
        addSubview(chip)
        chip.addSubview(star)
        chip.addSubview(labelStar)
        
        addSubview(name)
        addSubview(repository)
        
        avatar.anchor(top: self.topAnchor, left: self.leadingAnchor, bottom: nil, right: nil,
                      padding: .init(top: 5, left: 5, bottom: 5, right: 5),
                      size: .init(width: 50, height: 50))
        
        chip.anchor(top: self.topAnchor, left: nil, bottom: nil, right: self.trailingAnchor,
                    padding: .init(top: 5, left: 0, bottom: 5, right: -5),
                    size : .init(width : 70,height : 25))
        
        star.anchor(top: nil, left: chip.leadingAnchor, bottom: nil, right: nil,
                    padding: .init(top: 0, left: 10, bottom: 0, right: 0) )
        
        addConstraint(NSLayoutConstraint(item: star,
                                         attribute: .centerY,
                                         relatedBy: .equal,
                                         toItem: chip,
                                         attribute: .centerY,
                                         multiplier: 1,
                                         constant: 0))
        
        labelStar.anchor(top: star.topAnchor, left: star.trailingAnchor, bottom: nil, right: chip.trailingAnchor,
                         padding: .init(top: 2, left: 5, bottom: 0, right: 0) )
        
        name.anchor(top: self.topAnchor, left: avatar.trailingAnchor, bottom: nil, right: nil,
                    padding: .init(top: 5, left: 10, bottom: 5, right: 0),
                    size: .init(width: 120, height: 0))
        
        repository.anchor(top: name.bottomAnchor, left: avatar.trailingAnchor, bottom: nil, right: self.trailingAnchor,
                          padding: .init(top: 5, left: 10, bottom: 5, right: 0))
        
       
    }
}

extension UIView {
    
    func anchor(top : NSLayoutYAxisAnchor?,
                left:NSLayoutXAxisAnchor?,
                bottom:NSLayoutYAxisAnchor?,
                right:NSLayoutXAxisAnchor?,
                padding : UIEdgeInsets = .zero,
                size : CGSize = .zero){
        
        translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top{
            self.topAnchor.constraint(equalTo: top, constant: padding.top).isActive = true
        }
        if let left = left{
            self.leadingAnchor.constraint(equalTo: left, constant: padding.left).isActive = true
        }
        if let bottom = bottom{
            self.bottomAnchor.constraint(equalTo: bottom, constant: padding.bottom).isActive = true
        }
        if let right = right{
            self.trailingAnchor.constraint(equalTo: right, constant: padding.right).isActive = true
        }
        
        if size.width != 0{
            self.widthAnchor.constraint(equalToConstant: size.width).isActive = true
        }
        
        if size.height != 0{
            self.heightAnchor.constraint(equalToConstant: size.height).isActive = true
        }
        
        
    }
    
}
