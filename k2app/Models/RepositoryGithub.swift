import UIKit

class RepositoryGithub: NSObject {
    
    public var avatar : String
    public var name : String
    public var repository : String
    public var star : Int
    
    init(_ avatar : String,_ name : String = "",_ repository : String = "",_ star : Int = 0) {
        self.avatar = avatar
        self.name = name
        self.repository = repository
        self.star = star
    }
    
}
