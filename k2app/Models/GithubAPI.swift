import UIKit

class GithubAPI: NSObject {
    
    public static func getList(completion : @escaping(_ listRepository:Array<RepositoryGithub>) -> Void) {
        
        guard let url = URL(string: "https://api.github.com/search/repositories?q=language:swift&sort=stars") else{
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let json = try! JSONSerialization.data(withJSONObject: ["v":3], options: [])
        print(json)
        
        var listRepository : Array<RepositoryGithub> = []
        
        URLSession.shared.dataTask(with: url){ data,response,error in
            
            if let response = response {
                print(response)
            }
            
            if let data = data , error == nil {
                print(data)
                do{
                    let responseJSON = try JSONSerialization.jsonObject(with: data, options: [])
                    
                    if let resJson = responseJSON as? [String:Any] {
                        guard let listGithub = resJson["items"] as? Array<Dictionary<String,Any>> else{
                            return
                        }
                        
                        for repor in listGithub{
                            
                            let dictRepor = GithubAPI.refactorJsonGithub(dict: repor)
                            listRepository.append(dictRepor)
                        }
                        
                        completion(listRepository)
                        
                        guard let avatar = listGithub[0]["owner"] as? Dictionary<String,Any> else{
                            return
                        }
                        print(avatar["avatar_url"]!)
                    }
                    
                }catch{
                    print(error)
                }
                
            }
        }.resume()
    }
    
    public static func refactorJsonGithub(dict : Dictionary<String,Any>) -> RepositoryGithub{
        
        let name = dict["name"] as! String
        let repositoryName = dict["full_name"] as! String
        let stargazersCount = dict["stargazers_count"] as! Int
        let repository : RepositoryGithub = RepositoryGithub("https://avatars1.githubusercontent.com/u/3006190?v=4",name,repositoryName,stargazersCount)
        
        if let avatar = dict["owner"] as? Dictionary<String,Any>{
            repository.avatar = avatar["avatar_url"] as! String
        }
        
        return repository
    }
}
