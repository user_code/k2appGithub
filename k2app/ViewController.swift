import UIKit

class ViewController: UICollectionViewController , UICollectionViewDelegateFlowLayout {

    let search : UISearchController = UISearchController(searchResultsController: nil)
    let refresh : UIRefreshControl = UIRefreshControl()
    
    var listGithub : Array<RepositoryGithub> = []
    var searchListGithub : Array<RepositoryGithub> = []
    var searchActive = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.alwaysBounceVertical = true
        refresh.addTarget(self, action: #selector(refreshListGithub), for: .valueChanged)
        collectionView.addSubview(refresh)
        
        navigationItem.searchController = search
        search.searchBar.placeholder = "Search Name Github"
        search.searchBar.delegate = self
        
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.title = "List Github"
        navigationItem.largeTitleDisplayMode = .automatic
        
        collectionView.backgroundColor = .white
        collectionView.alwaysBounceVertical = true
        collectionView.register(CellGithub.self, forCellWithReuseIdentifier: "cellId")
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        addListRepository {
            self.collectionView.reloadData()
        }
    }
    
    func addListRepository(completion : @escaping() -> Void) {
        GithubAPI.getList{ (listRepository) in
            DispatchQueue.main.async {
                self.listGithub = listRepository
                completion()
            }
        }
    }
    
    @objc func refreshListGithub(){
        self.refresh.beginRefreshing()
        addListRepository {
            self.collectionView.reloadData()
            self.refresh.endRefreshing()
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(searchActive){
            return searchListGithub.count
        }
        return listGithub.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cellGitHub = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! CellGithub
        
        if(searchActive){
            let github = searchListGithub[indexPath.row]
            cellGitHub.github = github
        }else{
            let github = listGithub[indexPath.row]
            cellGitHub.github = github
        }
        
        return cellGitHub
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width, height: 60)
    }

}

extension ViewController : UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        searchListGithub = listGithub.filter({ $0.name.lowercased().prefix(searchText.count) == searchText.lowercased() })
        searchActive = true
        collectionView.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
        searchActive = false
        collectionView.reloadData()
    }
    
}
